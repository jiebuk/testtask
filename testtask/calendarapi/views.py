from django.contrib.auth.models import User
from django.http import HttpResponse
from django.db.models import Count
from .models import Event
from celery import shared_task
from rest_framework import viewsets, generics, status
from rest_framework.views import APIView
from calendarapi.serializers import UserSerializer, EventSerializer
from rest_framework.response import Response
# Create your views here.
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from .tasks import  send_mail_task
from rest_framework import permissions
from datetime import datetime, timedelta



class CustomAuthToken(ObtainAuthToken):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({

            'token': token.key,
            'user_id': user.pk,
            'email': user.email
        })

class EventList(APIView):

    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request, year, day, month, format=None):

        event = Event.objects.filter(date_event=year+'-'+month+'-'+day).filter(user__username=request.user)
        event_count = Event.objects.filter(date_event__month=month).filter(user__username=request.user).values('date_event__day').annotate(count_events=Count('date_event__day'))
        serializer = EventSerializer(event, many=True)
        return Response((serializer.data,list(event_count)))

class UserRegister(generics.CreateAPIView):
    serializer_class = UserSerializer

def get_date_send_email(date_event, time_start, reminder):
    date_send = datetime.combine(date_event, time_start)
    x = 0
    if reminder == 'one hour':
        x = 1
    if reminder == 'two hour':
        x = 2
    if reminder == 'four hour':
        x = 4
    if reminder == 'one day':
        x = 24
    if reminder == 'one week':
        x = 168
    date_send = date_send - timedelta(hours=x)
    return date_send


class CreateEvent(generics.CreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = EventSerializer

    def post(self, request, format=None):
        data = request.data#.dict()
        if not data['time_end'] or data['time_end']<data['time_start']:
            data['time_end'] = '24:00:PM'
        data['user'] = request.user.id

        serializer = EventSerializer(data=data)

        if serializer.is_valid():
            user = serializer.validated_data['user']
            reminder = serializer.validated_data['reminder']
            name_event = serializer.validated_data['name_event']

            #send_mail_task.delay(name_event, reminder, user.email)
            date_send = get_date_send_email(serializer.validated_data['date_event'], serializer.validated_data['time_start'], reminder)
            if date_send < datetime.now():
                return Response('pozdno)')
            send_mail_task.apply_async((name_event, reminder, user.email),eta=date_send)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
