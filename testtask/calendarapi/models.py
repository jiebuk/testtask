from django.db import models
from django.contrib.auth.models import User

# Create your models here.





class Event(models.Model):
    REMINDER_CHOICES = (
    ('one hour','За час'),
    ('two hour','За 2 часа'),
    ('four hour','За 4 часа'),
    ('one day','За День'),
    ('one week','За неделю')
    )
    name_event = models.CharField(max_length = 100)
    date_event = models.DateField()
    time_start = models.TimeField()
    time_end = models.TimeField(blank=True)
    reminder = models.CharField(max_length=10, choices=REMINDER_CHOICES)
    user = models.ForeignKey(User, on_delete = 'cascade')
