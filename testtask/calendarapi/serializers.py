from django.contrib.auth.models import User
from .models import Event
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email','password']
        write_only_fields = ('password')
    def create(self, validated_data):
        user = User(**validated_data)
        # Hash the user's password.
        user.set_password(validated_data['password'])
        user.save()
        return user

class EventSerializer(serializers.ModelSerializer):

    class Meta:
        model = Event
        fields = ['name_event', 'date_event', 'time_start', 'time_end', 'reminder','user']
        #read_only_fields = ('user',)


    def create(self, validated_data):
        event =Event(**validated_data)
        event.save()

        return event
