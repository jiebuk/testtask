from django.urls import path
from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('register/',views.UserRegister.as_view()),
    path('createevent/',views.CreateEvent.as_view()),
    url(r'^login/', views.CustomAuthToken.as_view()),
    url(r'^events/(?P<year>[0-9]{4})/(?P<day>[0-9]{2})/(?P<month>[0-9]{2})$', views.EventList.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
