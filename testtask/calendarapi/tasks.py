from celery import shared_task
from django.core.mail import send_mail
from time import sleep


@shared_task
def send_mail_task(event, reminder, email):

    send_mail('напоминание', event+" in an "+reminder , 'borisevichleva@fastmail.com', [email])
    return None
